import logging

from django.conf import settings
from django.core.cache import cache
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt

import requests

from autogm.exception_tracer import trace_exception
from game.models import Player, CharacterModifier
from . import dice


LOG = logging.getLogger(__name__)
CONFIG = settings.SLACK_CONFIG


class LookupError(Exception):
    def __init__(self, message, player=None, item=None):
        self.player = player
        self.item = item
        super().__init__(message)


def lookup(item, player):
    try:
        stats = cache.get('userstats')
    except:
        trace_exception()
        LOG.exception("Data missing from cache.")
        raise LookupError("No data available")

    try:
        playerstats = stats[player]
        if not playerstats:
            raise KeyError
    except KeyError as e:
        raise LookupError("No stats for player", player=player) from e

    try:
        stat = playerstats[item]
        if not item:
            raise KeyError
    except KeyError as e:
        raise LookupError("could not find item for player",
            player=player, item=item) from e

    return stat


def verify_slack_token(func):
    slack_verify_token = settings.SLACK_VERIFICATION_TOKEN

    def _wrap(request):
        token = request.POST.get('token', None)
        if token != slack_verify_token:
            raise PermissionDenied

        return func(request)

    return _wrap


def _command_response(text, response_type="ephemeral"):
    return JsonResponse({
        "respones_type": response_type,
        "text": text,
    })


@csrf_exempt
@verify_slack_token
def roll(request):
    user = request.POST.get('user_name', '')
    command = request.POST.get('text', '1d20')
    LOG.info("%s requested %s", user, command)

    try:
        roll = dice.parse(command, lookup, user)
    except dice.DiceRollParseError as parse_error:
        trace_exception()
        LOG.exception("Could not parse dice roll %s for %s", command, user)

        return _command_response(
            "Sorry -- we didn't understand what you meant: %s" % parse_error)
    except LookupError as lookup_error:
        trace_exception()
        LOG.exception("Could not perform attribute lookup")

        try:
            detail_options = [
                ["", " ({player})"],
                [" ({item})", " ({item} for {player})"]]
            details = detail_options[
                int(bool(lookup_error.item))][int(bool(lookup_error.player))]
            err_str = ("Sorry -- we couldn't look up that value: "
                    + str(lookup_error)
                    + details.format(item=lookup_error.item,
                                     player=lookup_error.player))
        except:
            err_str = "hehe"
            LOG.exception("You retard...")

        return _command_response(err_str)

    result = str(roll.value[0] if len(roll.value) == 1 else roll.value)
    verbose = '_{}_'.format(str(roll.verbose[:-1])) if roll.verbose else ''

    message = {
        "response_type": "in_channel",
        "text": "_{} rolled {}_:\n:game_die:  *{}*".format(
            user,
            command,
            result,
            ),
        "attachments": [
            {
                "text": verbose,
                "mrkdwn_in": ["title", "text"],
            },
        ],
    }

    try:
        requests.post(request.POST.get('response_url'), json=message)
    except:
        trace_exception()
        LOG.exception("Could not send request to slack: {}", message)

        return _command_response(
            "Sorry -- we're having trouble doing that right now.")

    return HttpResponse(status=200)


# TODO(kilogram): proper implementation of Slack API and errors
class SlackWebApiError(Exception):
    def __init__(self, reason, response=None):
        self.response = response
        super().__init__(self, reason)


def _send_turn_notification(from_user, to_user):
    slack_im_request = {
        "token": CONFIG.BOT_TOKEN,
        "username": CONFIG.BOT_USERNAME,
        "channel": "@%s" % to_user,
        "text": "Hi! %s wanted us to let you know it's your turn." % from_user,
    }

    response = requests.post(
            CONFIG.API_ENDPOINT + "chat.postMessage",
            params=slack_im_request)
    response.raise_for_status()
    slack_response = response.json()

    if not slack_response['ok']:
        raise SlackWebApiError(slack_response['error'], response=slack_response)


@csrf_exempt
@verify_slack_token
def turn(request):
    from_user = request.POST.get('user_name', '')
    to_user = request.POST.get('text', '').lstrip("@")

    if not to_user:
        return _command_response(
            "Whose turn is it? Let them know with /turn @<user>")

    try:
        _send_turn_notification(from_user, to_user)
    except requests.exceptions.RequestException:
        trace_exception()
        LOG.exception("/turn: Slack request failed")
    except SlackWebApiError as e:
        response = e.response

        if response.get("error", None) == "channel_not_found":
            return _command_response(
                "Sorry -- we're not sure who %s is" % to_notify)

        return _command_response(
            "Sorry -- we had trouble contacting %s" % to_notify)
    except:
        return _command_response(
            "Sorry -- we're having trouble doing that right now.")
    else:
        return _command_response("Got it -- we've let them know.")
