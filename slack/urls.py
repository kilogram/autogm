
from django.conf.urls import include, url
from django.contrib import admin

from . import views

urlpatterns = [
    url(r'roll/$', views.roll, name='roll'),
    url(r'turn/$', views.turn, name='turn'),
]
