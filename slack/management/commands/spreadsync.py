
import itertools as it
import json
import os

from django.core.management.base import BaseCommand, CommandError
from django.core.cache import cache

import gspread
from oauth2client.service_account import ServiceAccountCredentials


class Command(BaseCommand):
    def handle(self, *args, **options):
        creds = ServiceAccountCredentials.from_json_keyfile_dict(
                json.loads(os.environ['SYNC_ACCOUNT_CREDENTIALS']),
                scopes=['https://spreadsheets.google.com/feeds'])
        api = gspread.authorize(creds)

        docid = os.environ['SYNC_DOCUMENT']
        sheetname = os.environ['SYNC_SHEET']
        try:
            doc = api.open(docid)
        except gspread.SpreadsheetNotFound:
            doc = api.open_by_key(docid)

        sheet = doc.worksheet(sheetname)

        players = {
            key: val for (key, val) in
            # only read until last username
            it.takewhile(lambda x: x[0],
                # skip columns with no username specified
                it.dropwhile(lambda x: not x[0],
                    # Read the cell and return the value and column no.
                    ((sheet.cell(1, i).value, i)
                        for i in it.count(2))
                    )
                )
            }

        # Read descriptions of values
        items = {
            key.lower(): val for (key, val) in
            it.takewhile(lambda x: x[0],
                ((sheet.cell(i, 1).value, i)
                    for i in it.count(2))
                )
            }

        stats = {}
        for player, col in players.items():
            stats[player] = {}
            for item, row in items.items():
                stats[player][item] = sheet.cell(row, col).value

        print(cache.get('userstats', ''))
        cache.set('userstats', stats)
        cache.persist('userstats')
