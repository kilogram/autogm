
import contextlib
from heapq import nlargest as maxn, nsmallest as minn
import json
from nose.tools import assert_equal
import os.path as path
from unittest.mock import patch

from django.test import TestCase
from django.conf import settings

from . import dice


__all__ = (
        "TestDice",
    )


# Convenience functions
def _wrap(n, min, max):
    return min + (n % (max - min))


def d(s, n):
    return _wrap(n, 1, s)


class TestDice(object):

    DICE_TESTS = [
            ("1",                   lambda r: [1]),
            ("1+2",                 lambda r: [3]),
            ("3-5",                 lambda r: [-2]),
            ("-1+3",                lambda r: [2]),
            ("1+2*3",               lambda r: [7]),
            ("5/2+1",               lambda r: [3]),
            ("2*3+4*-5",            lambda r: [-14]),
            ("10+20,30+40",         lambda r: [30, 70]),
            ("30-40+3",             lambda r: [-7]),
            ("30-40p+3",            lambda r: [3]),
            ("50-40p+3",            lambda r: [13]),
            ("(1+2)*3",             lambda r: [9]),
            ("(1,2)*3",             lambda r: [3, 6]),
            ("(1,2)s*3",            lambda r: [9]),
            # ("(1,2),(3,4)",         lambda r: []),
            # ("2{3*5+4}+3",          lambda r: []),
            # ("0{3*5+4}+(1,2)",      lambda r: []),
            ("d20+3",               lambda r: [d(20, r[0]) + 3]),
            (
                "3{d20+3}^-1",
                lambda r: maxn(2, (d(20, n) + 3 for n in r[0:3])),
            ),
            # ("3d20^-1",             lambda r: [min(d(20, n) for n in r[0:3])]),
            ("0{}+3",               lambda r: [3]),
            ("[STR]+5",             lambda r: [7]),
            ("@somebody ([INT],3)", lambda r: [3, 3]),
            # ("10{d20} <= 10",       lambda r: []),
            # ("!10{d20-10}",         lambda r: []),
            # ("10{d20-10}**2",       lambda r: []),
            # ("10{d20-10}%2",        lambda r: []),
            # ("100{d20-10}%2",       lambda r: []),
            # ("2{[str]}",            lambda r: []),
            # ("2{[str]}m",           lambda r: []),
        ]

    def setUp(self):
        filename = path.join(settings.BASE_DIR, 'slack', 'random_ints.json')
        self.random_values = json.loads(open(filename).read())

    @contextlib.contextmanager
    def _stubs(self, lookup_dict=None):
        lookup_dict = lookup_dict or {
                'test_user': {
                    'int': 1,
                    'str': 2,
                },
                'somebody': {
                    'int': 3,
                },
            }
        def lookup(value, user):
            return lookup_dict[user][value]
        yield lookup

    def test_battery(self):
        for case, result, *optional in self.DICE_TESTS:
            yield self.check_dice_parse, case, result, optional

    def check_dice_parse(self, input, expected, rolls=None):
        with contextlib.ExitStack() as stack:
            lookup = stack.enter_context(self._stubs())
            mock_rand = stack.enter_context(patch('random.randint'))

            def make_randint(rolls):
                def randint(min, max):
                    return _wrap(next(rolls), min, max)
                return randint

            rolls = rolls[0] if rolls else self.random_values
            mock_rand.side_effect = make_randint(iter(rolls))
            roll = dice.parse(input, lookup, 'test_user')
            assert_equal(expected(rolls), roll.value)
