import re
import random as rand
from io import StringIO
from functools import reduce
from operator import mul
MAX_ITERATIONS = 5000

class DiceRollParseError(Exception):
    def __init__(self, msg):
        self.message = msg

class token:
    def __init__(self, i, v=0):
        self.id = i
        self.value = v

def listop(op,a,b):
    try:
        if len(a) == 0:
            return [op(0,b[i]) for i in range(len(b))]
        elif len(b) == 0:
            return [op(a[i],0) for i in range(len(a))]
        elif len(a) == 1:
            return [op(a[0],b[i]) for i in range(len(b))]
        elif len(b) == 1:
            return [op(a[i],b[0]) for i in range(len(a))]
        elif len(a) == len(b):
            return [op(a[i],b[i]) for i in range(len(a))]
        else:
            return [op(sum(a),sum(b))]
    except ZeroDivisionError as e:
        msg = 'Division by zero (%s/%s)' % (str(a), str(b))
        error = e
    except OverflowError as e:
        msg = 'Overflow error (%s, %s)' % (str(a), str(b))
        error = e
    except Exception as e:
        msg = 'Unknown error: %s' % e
        error = e
    raise DiceRollParseError(msg) from error

def rolldice(a,b):
    return [rand.randint(1,b) for _ in range(a)]

class parse:

    tokens = {}
    tokens['end'] = [0, None, None]
    tokens['num'] = [0,
            lambda value, s: value,
            None]
    tokens['str'] = [0,
            lambda value, s: parse(
                str(s.lookup_func(value,s.name)),s.name,s).value,
            None]
    tokens['nam'] = [0,
            lambda value, s: s.stateop(value),
            None]
    tokens['+'] = [20,
            lambda value, s: s.expression(100),
            lambda left, s: listop(lambda a,b: a+b,left,s.expression(20))]
    tokens['-'] = [20,
            lambda value, s: [-x for x in s.expression(100)],
            lambda left, s: listop(lambda a,b: a-b,left,s.expression(20))]
    tokens['*'] = tokens['x'] =[30,
            None,
            lambda left, s: listop(lambda a,b: a*b,left,s.expression(30))]
    tokens['/'] = [30,
            None,
            lambda left, s: listop(lambda a,b: a//b,left,s.expression(30))]
    tokens['%'] = [30,
            None,
            lambda left, s: listop(lambda a,b: a%b,left,s.expression(30))]
    tokens['**'] = [40,
            None,
            lambda left, s: listop(lambda a,b: a**b,left,s.expression(39))]
    tokens[','] = [2,
            None,
            lambda left, s: left + s.expression(2)]
    tokens['p'] = [10,
            None,
            lambda left, s: [max(0,x) for x in left]]
    tokens['s'] = [60,
            None,
            lambda left, s: [sum(left)]]
    tokens['m'] = [60,
            None,
            lambda left, s: [reduce(mul,left,1)]]
    tokens['('] = [150,
            lambda value, s: s.advance(s.expression(0),')'),
            None]
    tokens[')'] = [0,None,None]
    tokens['{'] = [150,
            None,
            lambda left, s: s.evalbrace(sum(left))]
    tokens['}'] = [0,None,None]
    tokens['d'] = [50,
            lambda value, s: s.evalroll(1,sum(s.expression(50))),
            lambda left, s: s.evalroll(sum(left),sum(s.expression(50)))]
    tokens['^'] = [45,
            None,
            lambda left, s: sorted(left)[-sum(s.expression(45)):]]
    tokens['_'] = [45,
            None,
            lambda left, s: sorted(left)[:sum(s.expression(45))]]
    tokens['>'] = [5,
            None,
            lambda left, s: listop(lambda a,b: 1 if a>b else 0,left,s.expression(5))]
    tokens['<'] = [5,
            None,
            lambda left, s: listop(lambda a,b: 1 if a<b else 0,left,s.expression(5))]
    tokens['=='] = [5,
            None,
            lambda left, s: listop(lambda a,b:  1 if a==b else 0,left,s.expression(5))]
    tokens['!='] = tokens['~='] = [5,
            None,
            lambda left, s: listop(lambda a,b:  1 if a!=b else 0,left,s.expression(5))]
    tokens['>='] = [5,
            None,
            lambda left, s: listop(lambda a,b:  1 if a>=b else 0,left,s.expression(5))]
    tokens['<='] = [5,
            None,
            lambda left, s: listop(lambda a,b:  1 if a<=b else 0,left,s.expression(5))]
    tokens['!'] = tokens['~'] = [100,
            lambda value, s: [0 if x > 0 else 1 for x in s.expression(100)],
            None]

    def __init__(self, program, lookup_func=None, name=None, parent=None):
        self.lookup_func = lookup_func
        if type(program) is str:
            self.next = self.tokenize(program.lower()).__next__
        else:
            self.next = iter(program).__next__
        self.token = self.next()
        self.name = name
        self.vio = StringIO() if parent is None else parent.vio
        self.iterlimit = [MAX_ITERATIONS] if parent is None else parent.iterlimit
        self.value = self.expression()
        if self.token.id != 'end':
            raise DiceRollParseError('Syntax error: '+str(self.token.value)+' is not operator')
        self.verbose = self.vio.getvalue()[:-1]
        if parent is None: self.vio.close()

    def tokenize(self, program):
        for name, number, string, operator in re.findall('\s*(?:(@[^\s]+)|(\d+)|(\[[^\]]+\])|(\*\*|(?:!|~|>|<|=)=|.))',program):

            if number:
                yield token('num',[int(number)])
            elif operator:
                yield token(operator)
            elif string:
                yield token('str',string[1:-1])
            elif name:
                yield token('nam',name[1:])
            else:
                raise DiceRollParseError('unknown operator: %r' % operator)
        yield token('end')

    def expression(self, rbp=0):
        self.iterlimit[0] = self.iterlimit[0] -1
        if self.iterlimit[0] < 0:
            raise DiceRollParseError('Max iterations reached')
        if self.token.id == 'end':
            raise DiceRollParseError('Reached end of string while parsing.')
        t = self.token
        self.token = self.next()
        try:
            left = self.tokens[t.id][1](t.value,self)
        except TypeError:
            raise DiceRollParseError('Invalid use of '+t.id+' operator')
        except KeyError:
            raise DiceRollParseError('Invalid operator: '+t.id)
        while rbp < self.tokens[self.token.id][0]:
            self.iterlimit[0] = self.iterlimit[0] -1
            if self.iterlimit[0] < 0:
                raise DiceRollParseError('Max iterations reached')
            t = self.token
            self.token = self.next()
            try:
                left = self.tokens[t.id][2](left,self)
            except TypeError:
                raise DiceRollParseError('Invalid use of '+t.id+' operator')
            except KeyError:
                raise DiceRollParseError('Invalid operator: '+t.id)
        return left

    def advance(self,left,sym):
        if self.token.id != sym:
            raise DiceRollParseError('Expected %r' % sym)
        self.token = self.next()
        return left

    def stateop(self, value):
        self.name = value
        return self.expression()

    def evalbrace(self, reps):
        tlist = []
        while self.token.id != '}':
            tlist = tlist + [self.token]
            self.token = self.next()
            if self.token.id == 'end':
                raise DiceRollParseError('Expected }')
        self.token = self.next()
        tlist = tlist + [token('end')]
        out = []
        for i in range(reps):
            out = out + parse(tlist,self.lookup_func,self.name,self).value
        return out

    def evalroll(self, a, b):
        sym = self.token.id
        rolls = rolldice(a,b)
        self.vio.write(str(a)+'d'+str(b)+' -> '+str(rolls)+', ')
        if sym == '^' or sym == '_':
            self.token = self.next()
            rolls = self.tokens[sym][2](rolls,self)
        return [sum(rolls)]
