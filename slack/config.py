import os


class SlackConfig(object):
    pass


def get_config(debug=False):
    config = SlackConfig()

    config.BOT_TOKEN = os.environ.get('SLACK_BOT_TOKEN', None)
    config.BOT_USERNAME = "autogm"

    if debug:
        config.API_ENDPOINT = os.environ.get(
                'DEBUG_SLACK_ENDPOINT', "http://localhost:16623/api/")
    else:
        config.API_ENDPOINT = "https://slack.com/api/"

    return config
