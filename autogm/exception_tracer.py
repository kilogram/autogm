import logging

import django
from django.core.exceptions import ImproperlyConfigured, MiddlewareNotUsed
from django.conf import settings

from raygun4py.raygunprovider import RaygunSender


def _create_sender():
    config = getattr(settings, 'RAYGUN4PY_CONFIG', {})
    apikey = getattr(settings,
                     'RAYGUN4PY_API_KEY',
                     config.get('api_key', None))
    if not apikey:
        raise ImproperlyConfigured("No Raygun API key specified")

    return RaygunSender(apikey, config=config)


class ExceptionTracer(object):

    sender = None

    def __init__(self, get_response):
        self._get_response = get_response

        if settings.DEBUG:
            raise MiddlewareNotUsed

        elif not ExceptionTracer.sender:
            ExceptionTracer.sender = _create_sender()

    def __call__(self, request):
        return self._get_response(request)

    def _read_request(self, req):
        return {
                'hostName': req.get_host(),
                'url': req.path,
                'httpMethod': req.method,
                'ipAddress': req.META.get('REMOTE_ADDR', '?'),
                'queryString': {key: val for key in req.GET.items()},
                'form': {key: val for key, val in req.POST.items()},
                'headers': {key: val for key, val in req.META.items()
                            if not key.startswith('wsgi')},
                'rawData': getattr(
                    req, 'body', getattr(req, 'raw_post_data', {})),
            }

    def process_exception(self, request, exception):
        self.sender.send_exception(
                exception=exception,
                request=self._read_request(request),
                extra_environment_data={
                    'frameworkVersion': django.get_version(),
                },
            )

    def trace_exception(self):
        sender.send_exception()


if settings.DEBUG:
    def trace_exception():
        pass
else:
    def trace_exception():
        if not ExceptionTracer.sender:
            ExceptionTracer.sender = _create_sender()

        ExceptionTracer.sender.send_exception()
