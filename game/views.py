from django.shortcuts import render
from django.http import HttpResponse

from .models import CharacterModifier


def index(request):
    return render(request, 'index.html')


def db(request):
    char = CharacterModifier.objects.all()
    return render(request, 'db.html', {'characters': char})
