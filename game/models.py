from django.db import models


i = models.SmallIntegerField
class CharacterModifier(models.Model):
    # Attributes
    STR = i('str')
    DEX = i('dex')
    CON = i('con')
    INT = i('int')
    WIS = i('wis')
    CHA = i('cha')
    # Saves
    STR_SAVE = i('str_save')
    DEX_SAVE = i('dex_save')
    CON_SAVE = i('con_save')
    INT_SAVE = i('int_save')
    WIS_SAVE = i('wis_save')
    CHA_SAVE = i('cha_save')
    # Skills
    acrobatics = i()
    animal_handling = i()
    arcana = i()
    althetics = i()
    deception = i()
    history = i()
    insight = i()
    intimidation = i()
    investigation = i()
    medicine = i()
    nature = i()
    preception = i()
    performance = i()
    persuasion = i()
    religion = i()
    sleight_of_hand = i()
    stealth = i()
    survival = i()
    initiative = i()


class Player(models.Model):
    username = models.CharField(max_length=255)
    modifiers = models.OneToOneField(
            CharacterModifier,
        )
